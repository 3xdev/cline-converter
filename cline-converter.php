<?php
// if this file is accesed directly, do not load (this prevents malicious access)
defined( 'ABSPATH' ) || exit;

/*
Plugin Name: Cline Converter
Plugin URI: https://3xweb.site
Description: Embeds a CCcam to OScam converter on your website using the [camconverter] shortcode
Version: 1.0
Author: Douglas de Araújo
Author URI: https://3web.site
License: Kopimi
*/

// if for some reason this is called from admin it does nothing
if (is_admin()) return;

/**
 * Loads a public script
 */
function xweb_cline_converter_script($hook) {
    
    // create my own version codes
    $my_js_ver  = date("ymd-Gis", filemtime( plugin_dir_path( __FILE__ ) . '/resources/js/' ));
        
    // 
    wp_enqueue_script( 'xweb_conversion_script', plugins_url( '/resources/js/script.js', __FILE__ ), array('jquery'), $my_js_ver );
    wp_enqueue_script( 'clipboard', 'https://cdnjs.cloudflare.com/ajax/libs/clipboard.js/2.0.10/clipboard.min.js', array(), $my_js_ver, true );
}
add_action('wp_enqueue_scripts', 'xweb_cline_converter_script');

// this is the main function that returns our [camconverter] shortcode content
// You will notice I'm putting html between php tags, this way i can show html without having to write in a single long string
function camconverter_function( $atts ){
    ?>
        <style>
        #copyarea {
            position: relative;
        }
        #copyarea textarea {
            width: 100%;
            height: 100%;
            box-sizing: border-box;
        }
        #copyarea button {
            position: absolute;
            top: 0;
            right: 0;
        }
        #convert{
            margin-bottom: 2%;
        }
        </style>
        <textarea id="caminput" name="caminput" cols="50" rows="8" placeholder=" Insert your CCcam lines or OScam lines here"></textarea>
        <button id="convert">Convert</button>
        <div id="copyarea">
            <button id="copybutton"data-clipboard-target="#camoutput">✂️ Copy</button> 
            <textarea id="camoutput" type="text" readonly="true" cols="50" rows="8"></textarea>
        </div>
    <?php
}
// here we ask wordpress to include our shortcode by its unique name and provide its callback function
add_shortcode( 'camconverter', 'camconverter_function' );